﻿using System.Threading.Tasks;
using SignalR.Chat.Data.Services.Abstracts;
using SignalR.Chat.Models.Requests;
using SignalR.Chat.Models.Responses;
using SignalR.Chat.Services.Abstracts;

namespace SignalR.Chat.Services
{
    public class ChatService : IChatService
    {
        private readonly IChatDataService _chatDataService;

        public ChatService(IChatDataService chatDataService)
        {
            _chatDataService = chatDataService;
        }

        public async Task<int> CreateChat(CreateChatRequest request)
        {
            var chatId = await _chatDataService.CreateChat(request);
            return chatId;
        }

        public async Task<GetChatResponse> GetChatById(int chatId)
        {
            var chat = await _chatDataService.GetChatById(chatId);
            return chat;
        }
    }
}