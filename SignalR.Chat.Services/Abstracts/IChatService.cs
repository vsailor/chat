﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SignalR.Chat.Models.Requests;
using SignalR.Chat.Models.Responses;

namespace SignalR.Chat.Services.Abstracts
{
    public interface IChatService
    {
        Task<int> CreateChat(CreateChatRequest request);
        Task<GetChatResponse> GetChatById(int chatId);
        Task<IEnumerable<GetChatResponse>> GetAllChats();
    }
}