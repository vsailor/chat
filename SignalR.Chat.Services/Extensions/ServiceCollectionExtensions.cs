﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SignalR.Chat.Data.Extensions;
using SignalR.Chat.Services.Abstracts;

namespace SignalR.Chat.Services.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void RegisterServiceDependencies(this IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddTransient<IChatService, ChatService>();

            services.RegisterDataDependencies(configuration);
        }
    }
}