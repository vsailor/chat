﻿namespace SignalR.Chat.Models.Responses
{
    public class GetUserResponse
    {
        public int Id { get; set; }

        public int ChatId { get; set; }

        public bool IsBanned { get; set; }

        public bool IsModerator { get; set; }

        public string Email { get; set; }

        public string Metadata { get; set; }
    }
}