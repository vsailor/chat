﻿namespace SignalR.Chat.Models.Responses
{
    public class GetAllChatsResponse
    {
        public int Id { get; set; }

        public string ChatName { get; set; }

        public int MaxMessages { get; set; }

        public bool AutoApproveMessages { get; set; }
    }
}