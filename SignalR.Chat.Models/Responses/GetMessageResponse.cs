﻿using System;

namespace SignalR.Chat.Models.Responses
{
    public class GetMessageResponse
    {
        public int Id { get; set; }

        public string Body { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsPublished { get; set; }

        public int CreatedBy { get; set; }

        public int ChatId { get; set; }

        public bool IsModerator { get; set; }

        public bool IsDeleted { get; set; }

        public string Email { get; set; }

        public string Metadata { get; set; }

        public bool IsBanned { get; set; }
    }
}