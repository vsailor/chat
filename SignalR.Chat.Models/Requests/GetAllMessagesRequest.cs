﻿using System;

namespace SignalR.Chat.Models.Requests
{
    public class GetAllMessagesRequest
    {
        public int ChatId { get; set; }
        public bool OnlyPublished { get; set; }
        public int UserId { get; set; }
        public DateTime? StartDate { get; set; }
    }
}