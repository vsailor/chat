﻿using System.Collections.Generic;

namespace SignalR.Chat.Models.Requests
{
    public class UpdateChatRequest
    {
        public int Id { get; set; }

        public string ChatName { get; set; }

        public int? MaxMessages { get; set; }

        public bool? AutoApproveMessages { get; set; }

        public IEnumerable<string> ModeratorEmails { get; set; }
    }
}