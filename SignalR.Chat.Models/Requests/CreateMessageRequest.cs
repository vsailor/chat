﻿using System;

namespace SignalR.Chat.Models.Requests
{
    public class CreateMessageRequest
    {
        public CreateMessageRequest(int createdBy, string body, int chatId)
        {
            CreatedBy = createdBy;
            Body = body;
            ChatId = chatId;
            CreatedOn = DateTime.UtcNow;
        }

        public int CreatedBy { get; set; }
        
        public string Body { get; set; }

        public int ChatId { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}