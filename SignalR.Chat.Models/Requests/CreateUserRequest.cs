﻿namespace SignalR.Chat.Models.Requests
{
    public class CreateUserRequest
    {
        public int ChatId { get; set; }

        public bool IsBanned { get; set; }

        public bool IsModerator { get; set; }

        public string Email { get; set; }

        public string Metadata { get; set; }
    }
}