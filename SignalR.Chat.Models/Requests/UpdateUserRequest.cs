﻿namespace SignalR.Chat.Models.Requests
{
    public class UpdateUserRequest
    {
        public int Id { get; set; }

        public bool IsModerator { get; set; }

        public string Metadata { get; set; }
    }
}