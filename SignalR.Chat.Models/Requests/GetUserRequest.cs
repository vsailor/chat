﻿namespace SignalR.Chat.Models.Requests
{
    public class GetUserRequest
    {
        public GetUserRequest(string email, int chatId)
        {
            Email = email;
            ChatId = chatId;
        }

        public string Email { get; set; }

        public int ChatId { get; set; }
    }
}