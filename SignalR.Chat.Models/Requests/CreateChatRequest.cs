﻿namespace SignalR.Chat.Models.Requests
{
    public class CreateChatRequest
    {
        public string ChatName { get; set; }

        public bool? AutoApproveMessages { get; set; }

        public int? MaxMessages { get; set; }

        public string[] ModeratorEmails { get; set; }
    }
}