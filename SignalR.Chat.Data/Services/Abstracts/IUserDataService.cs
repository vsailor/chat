﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SignalR.Chat.Models.Requests;
using SignalR.Chat.Models.Responses;

namespace SignalR.Chat.Data.Services.Abstracts
{
    public interface IUserDataService
    {
        Task<GetUserResponse> GetUser(GetUserRequest request);
        Task<GetUserResponse> GetUserById(int userId);
        Task<IEnumerable<GetUserResponse>> GetAllUsers(int chatId);
        Task<GetUserResponse> CreateUser(CreateUserRequest request);
        Task SetUserIsBannedStatus(int userId, bool isBanned);
        Task UpdateUser(UpdateUserRequest request);
    }
}