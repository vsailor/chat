﻿namespace SignalR.Chat.Data.Services.Abstracts
{
    public abstract class BaseDataService
    {
        protected string ConnectionString { get; set; }

        protected BaseDataService(string connectionString)
        {
            ConnectionString = connectionString;
        }
    }
}