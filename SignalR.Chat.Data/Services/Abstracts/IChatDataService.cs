﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SignalR.Chat.Models.Requests;
using SignalR.Chat.Models.Responses;

namespace SignalR.Chat.Data.Services.Abstracts
{
    public interface IChatDataService
    {
        Task<int> CreateChat(CreateChatRequest request);
        Task<GetChatResponse> GetChatById(int chatId);
        Task<IEnumerable<GetAllChatsResponse>> GetAllChats();
        Task DeleteChat(int chatId);
        Task UpdateChat(UpdateChatRequest request);
        Task<IEnumerable<GetMessageResponse>> GetAllMessages(GetAllMessagesRequest request);
        Task<int> AddMessage(CreateMessageRequest request);
        Task DeleteMessage(int id);
        Task<IEnumerable<int>> DeleteAllMessages(int userId);
        Task<GetMessageResponse> GetMessageById(int id);
        Task PublishMessage(int messageId);
        Task DeleteMessages(int chatId);
    }
}