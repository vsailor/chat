﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SignalR.Chat.Common.Options;
using SignalR.Chat.Data.Exceptions;
using SignalR.Chat.Data.Repositories.Abstracts;
using SignalR.Chat.Data.Services.Abstracts;
using SignalR.Chat.Models.Requests;
using SignalR.Chat.Models.Responses;

namespace SignalR.Chat.Data.Services
{
    public class UserDataService : BaseDataService, IUserDataService
    {
        private readonly IUserRepository _userRepository;
        private readonly IChatRepository _chatRepository;

        public UserDataService(IUserRepository userRepository, IChatRepository chatRepository, IOptions<AppConfigOptions> configuration) 
            : base(configuration.Value.DbConnectionString)
        {
            _userRepository = userRepository;
            _chatRepository = chatRepository;
        }

        public async Task<GetUserResponse> GetUser(GetUserRequest request)
        {
            await using var conn = new SqlConnection(ConnectionString);
            var user = await _userRepository.GetUser(conn, request.Email, request.ChatId);
            return user;
        }

        public async Task<GetUserResponse> GetUserById(int userId)
        {
            await using var conn = new SqlConnection(ConnectionString);
            var user = await _userRepository.GetUserById(conn, userId);
            return user;
        }

        public async Task<IEnumerable<GetUserResponse>> GetAllUsers(int chatId)
        {
            await using var conn = new SqlConnection(ConnectionString);
            var users = await _userRepository.GetAllUsers(conn, chatId);
            return users;
        }

        public async Task<GetUserResponse> CreateUser(CreateUserRequest request)
        {
            await using var conn = new SqlConnection(ConnectionString);
            var existingUser = await _userRepository.GetUser(conn, request.Email, request.ChatId);
            if (existingUser != null)
                throw new UserAlreadyExistException(existingUser.Email);

            var chat = await _chatRepository.GetChatById(conn, request.ChatId);
            if (chat == null)
                throw new ChatNotFoundException(request.ChatId);

            var userId = await _userRepository.CreateUser(conn, request);
            var user = await _userRepository.GetUserById(conn, userId);
            return user;
        }

        public async Task SetUserIsBannedStatus(int userId, bool isBanned)
        {
            await using var conn = new SqlConnection(ConnectionString);
            int rowsUpdated = await _userRepository.SetUserIsBannedStatus(conn, userId, isBanned);
            if (rowsUpdated == 0)
                throw new UserNotFoundException(userId);
        }

        public async Task UpdateUser(UpdateUserRequest request)
        {
            await using var conn = new SqlConnection(ConnectionString);
            int rowsUpdated = await _userRepository.UpdateUser(conn, request);
            if (rowsUpdated == 0)
                throw new UserNotFoundException(request.Id);
        }
    }
}