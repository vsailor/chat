﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SignalR.Chat.Common.Options;
using SignalR.Chat.Data.Exceptions;
using SignalR.Chat.Data.Repositories.Abstracts;
using SignalR.Chat.Data.Services.Abstracts;
using SignalR.Chat.Models.Requests;
using SignalR.Chat.Models.Responses;

namespace SignalR.Chat.Data.Services
{
    public class ChatDataService : BaseDataService, IChatDataService
    {
        private readonly IChatRepository _chatRepository;
        private readonly IUserRepository _userRepository;

        public ChatDataService(IChatRepository chatRepository, IUserRepository userRepository, IOptions<AppConfigOptions> configuration) 
            : base(configuration.Value.DbConnectionString)
        {
            _chatRepository = chatRepository;
            _userRepository = userRepository;
        }

        public async Task<int> CreateChat(CreateChatRequest request)
        {
            await using var conn = new SqlConnection(ConnectionString);
            conn.Open();
            var existingChat = await _chatRepository.GetChatByName(conn, request.ChatName);
            if (existingChat != null)
                throw new ChatAlreadyExistException(existingChat.ChatName);

            int chatId;
            await using var tran = conn.BeginTransaction();

            try
            {
                chatId = await _chatRepository.CreateChat(conn, tran, request);
                await AddModerators(chatId, request.ModeratorEmails, new List<GetUserResponse>(), conn, tran);

                tran.Commit();
            }
            catch
            {
                tran.Rollback();
                throw;
            }

            return chatId;
        }

        public async Task<GetChatResponse> GetChatById(int chatId)
        {
            await using var conn = new SqlConnection(ConnectionString);
            var chat = await _chatRepository.GetChatById(conn, chatId);
            var emails = await _chatRepository.GetModeratorEmails(conn, chatId);
            chat.ModeratorEmails = emails;
            return chat;
        }

        public async Task<IEnumerable<GetAllChatsResponse>> GetAllChats()
        {
            await using var conn = new SqlConnection(ConnectionString);
            var chats = await _chatRepository.GetAllChats(conn);
            return chats;
        }

        public async Task DeleteChat(int chatId)
        {
            await using var conn = new SqlConnection(ConnectionString);
            conn.Open();
            var chat = await _chatRepository.GetChatById(conn, chatId);
            if (chat == null)
                throw new ChatNotFoundException(chatId);

            await using var tran = conn.BeginTransaction();

            try
            {
                await _chatRepository.DeleteAllChatMessages(conn, tran, chatId);
                await _userRepository.DeleteAllChatUsers(conn, tran, chatId);
                await _chatRepository.DeleteChat(conn, tran, chatId);

                tran.Commit();
            }
            catch
            {
                tran.Rollback();
                throw;
            }
        }

        public async Task UpdateChat(UpdateChatRequest request)
        {
            await using var conn = new SqlConnection(ConnectionString);
            conn.Open();

            var existingChat = await _chatRepository.GetChatByName(conn, request.ChatName);
            if (existingChat != null && existingChat.ChatName != request.ChatName)
                throw new ChatAlreadyExistException(existingChat.ChatName);

            var users = await _userRepository.GetAllUsers(conn, request.Id);

            await using var tran = conn.BeginTransaction();

            try
            {
                int rowsUpdated = await _chatRepository.UpdateChat(conn, tran, request);
                if (rowsUpdated == 0)
                    throw new ChatNotFoundException(request.Id);

                await RemoveModerators(users, request.ModeratorEmails, conn, tran);
                await _chatRepository.SetModerators(request.ModeratorEmails, request.Id, true, conn, tran);
                await AddModerators(request.Id, request.ModeratorEmails, users, conn, tran);

                tran.Commit();
            }
            catch
            {
                tran.Rollback();
                throw;
            }
        }

        private async Task RemoveModerators(
            IEnumerable<GetUserResponse> allUsers, 
            IEnumerable<string> newModerators, 
            SqlConnection conn, 
            SqlTransaction tran)
        {
            var newNonModerators = 
                allUsers.Where(u => u.IsModerator && !newModerators.Contains(u.Email))
                    .Select(u => u.Id);

            await _chatRepository.SetModerators(newNonModerators, false, conn, tran);
        }

        private async Task AddModerators(int chatId, IEnumerable<string> emails, IEnumerable<GetUserResponse> allUsers,
            SqlConnection conn, SqlTransaction tran)
        {
            var unknownEmails = emails.Where(email => allUsers.All(u => u.Email != email));
            foreach (var email in unknownEmails)
            {
                await _userRepository.CreateUser(conn, tran, new CreateUserRequest
                {
                    IsBanned = false,
                    Email = email,
                    IsModerator = true,
                    ChatId = chatId
                });
            }
        }

        public async Task<IEnumerable<GetMessageResponse>> GetAllMessages(GetAllMessagesRequest request)
        {
            await using var conn = new SqlConnection(ConnectionString);
            var messages = await _chatRepository.GetAllMessages(conn, request);
            return messages;
        }

        public async Task<int> AddMessage(CreateMessageRequest request)
        {
            await using var conn = new SqlConnection(ConnectionString);
            var chat = await _chatRepository.GetChatById(conn, request.ChatId);
            var messageId = await _chatRepository.AddMessage(conn, request, chat.AutoApproveMessages);
            return messageId;
        }

        public async Task DeleteMessage(int id)
        {
            await using var conn = new SqlConnection(ConnectionString);
            var rowsDeleted = await _chatRepository.DeleteMessage(conn, id);
            if (rowsDeleted == 0)
                throw new MessageNotFoundException(id);
        }

        public async Task<IEnumerable<int>> DeleteAllMessages(int userId)
        {
            await using var conn = new SqlConnection(ConnectionString);
            var messages = await _chatRepository.GetUserMessages(conn, userId);
            await _chatRepository.DeleteUserMessages(conn, userId);

            return messages;
        }

        public async Task<GetMessageResponse> GetMessageById(int id)
        {
            await using var conn = new SqlConnection(ConnectionString);
            var message = await _chatRepository.GetMessageById(conn, id);
            return message;
        }

        public async Task PublishMessage(int messageId)
        {
            await using var conn = new SqlConnection(ConnectionString);
            var message = await _chatRepository.GetMessageById(conn, messageId);
            if (message == null)
                throw new MessageNotFoundException(messageId);

            if (message.IsPublished)
                return;

            await _chatRepository.PublishMessage(conn, messageId);
        }

        public async Task DeleteMessages(int chatId)
        {
            await using var conn = new SqlConnection(ConnectionString);
            var chat = await _chatRepository.GetChatById(conn, chatId);
            if (chat == null)
                throw new ChatNotFoundException(chatId);

            await _chatRepository.DeleteAllMessages(conn, chatId);
        }
    }
}