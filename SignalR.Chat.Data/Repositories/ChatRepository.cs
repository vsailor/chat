﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using SignalR.Chat.Data.Repositories.Abstracts;
using SignalR.Chat.Models.Requests;
using SignalR.Chat.Models.Responses;

namespace SignalR.Chat.Data.Repositories
{
    public class ChatRepository : IChatRepository
    {
        public async Task<GetAllChatsResponse> GetChatByName(SqlConnection conn, string name)
        {
            var query = "SELECT Id, ChatName, MaxMessages, AutoApproveMessages FROM Chats WHERE ChatName = @name";
            var response = await conn.QueryFirstOrDefaultAsync<GetAllChatsResponse>(query, new { name });
            return response;
        }

        public async Task<GetChatResponse> GetChatById(SqlConnection conn, int id)
        {
            var query = "SELECT Id, ChatName, MaxMessages, AutoApproveMessages FROM Chats WHERE Id = @id";
            var response = await conn.QueryFirstOrDefaultAsync<GetChatResponse>(query, new { id });
            return response;
        }

        public async Task<int> CreateChat(SqlConnection conn, SqlTransaction tran, CreateChatRequest request)
        {
            var query = @"INSERT INTO Chats(ChatName, MaxMessages, AutoApproveMessages)
                          OUTPUT INSERTED.Id as int
                          VALUES (@chatName, @maxMessages, @autoApproveMessages)";

            var response = await conn.QuerySingleAsync<int>(query, request, tran);
            return response;
        }

        public async Task<IEnumerable<GetAllChatsResponse>> GetAllChats(SqlConnection conn)
        {
            var query = "SELECT Id, ChatName, MaxMessages, AutoApproveMessages FROM Chats";
            var response = await conn.QueryAsync<GetAllChatsResponse>(query);
            return response;
        }

        public async Task<int> DeleteChat(SqlConnection conn, SqlTransaction tran, int chatId)
        {
            int count = await conn.ExecuteAsync("DELETE FROM Chats WHERE Id = @chatId", new { chatId }, tran);
            return count;
        }

        public async Task<int> UpdateChat(SqlConnection conn, SqlTransaction tran, UpdateChatRequest request)
        {
            int count = await conn.ExecuteAsync("UPDATE Chats SET ChatName = @chatName, MaxMessages = @maxMessages, AutoApproveMessages = @autoApproveMessages WHERE Id = @id",
                request, tran);
            return count;
        }

        public async Task<IEnumerable<GetMessageResponse>> GetAllMessages(SqlConnection conn, GetAllMessagesRequest request)
        {
            var query = new SqlBuilder();
            var template = query.AddTemplate(
                @"SELECT m.Id, m.Body, m.IsDeleted, m.CreatedOn, m.CreatedBy, m.IsPublished, u.IsModerator, u.Email, u.Metadata, u.IsBanned FROM Messages m
                JOIN Users u ON m.CreatedBy = u.Id /**where**/ /**orderby**/");

            query.Where("m.ChatId = @chatId", new { chatId = request.ChatId });

            if (request.OnlyPublished)
            {
                query.Where("m.IsDeleted = 0")
                    .Where("(m.IsPublished = 1 OR m.CreatedBy = @createdBy)", new { createdBy = request.UserId });
            }

            if (request.StartDate.HasValue)
                query.Where("m.CreatedOn >= @startDate", new { startDate = request.StartDate.Value.ToUniversalTime() });

            query.OrderBy("m.CreatedOn ASC");

            var response = await conn.QueryAsync<GetMessageResponse>(template.RawSql, template.Parameters);
            return response;
        }

        public async Task<int> AddMessage(SqlConnection conn, CreateMessageRequest request, bool publish)
        {
            var query = @"INSERT INTO Messages(Body, IsDeleted, ChatId, CreatedBy, isPublished, CreatedOn)
                          OUTPUT INSERTED.Id as int
                          VALUES (@body, 0, @chatId, @createdBy, @publish, @createdOn)";

            var response = await conn.QuerySingleAsync<int>(query, new
            {
                body = request.Body,
                chatId = request.ChatId,
                createdBy = request.CreatedBy,
                publish,
                createdOn = request.CreatedOn
            });

            return response;
        }

        public async Task<GetMessageResponse> GetMessageById(SqlConnection conn, int messageId)
        {
            var query = @"
                SELECT m.Id, m.Body, m.CreatedOn, m.CreatedBy, m.ChatId, m.IsPublished, u.IsModerator, u.Email, u.Metadata, u.IsBanned FROM Messages m
                JOIN Users u ON m.CreatedBy = u.Id
                WHERE m.Id = @messageId ORDER BY m.CreatedOn DESC";
            var response = await conn.QueryFirstOrDefaultAsync<GetMessageResponse>(query, new { messageId });
            return response;
        }

        public async Task<int> DeleteMessage(SqlConnection conn, int messageId)
        {
            int count = await conn.ExecuteAsync("UPDATE Messages SET IsDeleted = 1 WHERE Id = @messageId", new { messageId });
            return count;
        }

        public async Task<IEnumerable<int>> GetUserMessages(SqlConnection conn, int userId)
        {
            var query = "SELECT Id FROM Messages WHERE CreatedBy = @userId";
            var response = await conn.QueryAsync<int>(query, new { userId });
            return response;
        }

        public async Task<int> DeleteUserMessages(SqlConnection conn, int userId)
        {
            int count = await conn.ExecuteAsync("UPDATE Messages SET IsDeleted = 1 WHERE CreatedBy = @userId", new { userId });
            return count;
        }

        public async Task DeleteAllChatMessages(SqlConnection conn, SqlTransaction tran, int chatId)
        {
            await conn.ExecuteAsync("DELETE FROM Messages WHERE ChatId = @chatId", new { chatId }, tran);
        }

        public async Task PublishMessage(SqlConnection conn, int messageId)
        {
            await conn.ExecuteAsync("UPDATE Messages SET IsPublished = 1 WHERE Id = @messageId", new { messageId });
        }

        public async Task<IEnumerable<string>> GetModeratorEmails(SqlConnection conn, int chatId)
        {
            var query = "SELECT Email FROM Users WHERE ChatId = @chatId AND IsModerator = 1";
            var response = await conn.QueryAsync<string>(query, new { chatId });
            return response;
        }

        public async Task SetModerators(IEnumerable<int> users, bool isModerator, SqlConnection conn, SqlTransaction tran)
        {
            await conn.ExecuteAsync("UPDATE Users SET IsModerator = @isModerator WHERE Id IN @users", new { isModerator, users }, tran);
        }

        public async Task SetModerators(IEnumerable<string> users, int chatId, bool isModerator, SqlConnection conn, SqlTransaction tran)
        {
            await conn.ExecuteAsync("UPDATE Users SET IsModerator = @isModerator WHERE Email IN @users AND ChatId = @chatId", 
                new { isModerator, users, chatId }, tran);
        }

        public async Task DeleteAllMessages(SqlConnection conn, int chatId)
        {
            await conn.ExecuteAsync("DELETE FROM Messages WHERE ChatId = @chatId", new {chatId});
        }
    }
}