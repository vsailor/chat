﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using SignalR.Chat.Models.Requests;
using SignalR.Chat.Models.Responses;

namespace SignalR.Chat.Data.Repositories.Abstracts
{
    public interface IUserRepository
    {
        Task<GetUserResponse> GetUser(SqlConnection conn, string email, int chatId);
        Task<IEnumerable<GetUserResponse>> GetAllUsers(SqlConnection conn, int chatId);
        Task<GetUserResponse> GetUserById(SqlConnection conn, int userId);
        Task<int> CreateUser(SqlConnection conn, CreateUserRequest request);
        Task<int> CreateUser(SqlConnection conn, SqlTransaction tran, CreateUserRequest request);
        Task<int> SetUserIsBannedStatus(SqlConnection conn, int userId, bool isBanned);
        Task DeleteAllChatUsers(SqlConnection conn, SqlTransaction tran, int chatId);
        Task<int> UpdateUser(SqlConnection conn, UpdateUserRequest request);
    }
}