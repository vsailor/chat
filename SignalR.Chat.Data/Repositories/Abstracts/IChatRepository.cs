﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using SignalR.Chat.Models.Requests;
using SignalR.Chat.Models.Responses;

namespace SignalR.Chat.Data.Repositories.Abstracts
{
    public interface IChatRepository
    {
        Task<GetAllChatsResponse> GetChatByName(SqlConnection conn, string name);
        Task<GetChatResponse> GetChatById(SqlConnection conn, int id);
        Task<int> CreateChat(SqlConnection conn, SqlTransaction tran, CreateChatRequest request);
        Task<IEnumerable<GetAllChatsResponse>> GetAllChats(SqlConnection conn);
        Task<int> DeleteChat(SqlConnection conn, SqlTransaction tran, int chatId);
        Task<int> UpdateChat(SqlConnection conn, SqlTransaction tran, UpdateChatRequest request);
        Task<IEnumerable<GetMessageResponse>> GetAllMessages(SqlConnection conn, GetAllMessagesRequest request);
        Task<int> AddMessage(SqlConnection conn, CreateMessageRequest request, bool publish);
        Task<GetMessageResponse> GetMessageById(SqlConnection conn, int messageId);
        Task<IEnumerable<int>> GetUserMessages(SqlConnection conn, int userId);
        Task<int> DeleteMessage(SqlConnection conn, int messageId);
        Task<int> DeleteUserMessages(SqlConnection conn, int userId);
        Task DeleteAllChatMessages(SqlConnection conn, SqlTransaction tran, int chatId);
        Task PublishMessage(SqlConnection conn, int messageId);
        Task<IEnumerable<string>> GetModeratorEmails(SqlConnection conn, int chatId);
        Task SetModerators(IEnumerable<int> users, bool isModerator, SqlConnection conn, SqlTransaction tran);
        Task SetModerators(IEnumerable<string> users, int chatId, bool isModerator, SqlConnection conn, SqlTransaction tran);
        Task DeleteAllMessages(SqlConnection conn, int chatId);
    }
}