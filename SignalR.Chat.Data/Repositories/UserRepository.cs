﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using SignalR.Chat.Data.Repositories.Abstracts;
using SignalR.Chat.Models.Requests;
using SignalR.Chat.Models.Responses;

namespace SignalR.Chat.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        public async Task<GetUserResponse> GetUser(SqlConnection conn, string email, int chatId)
        {
            var query = "SELECT Id, ChatId, IsBanned, IsModerator, Email, Metadata FROM Users WHERE Email = @email AND ChatId = @chatId";
            var response = await conn.QueryFirstOrDefaultAsync<GetUserResponse>(query, new {email, chatId});
            return response;
        }

        public async Task<IEnumerable<GetUserResponse>> GetAllUsers(SqlConnection conn, int chatId)
        {
            var query = "SELECT Id, ChatId, IsBanned, IsModerator, Email, Metadata FROM Users WHERE ChatId = @chatId";
            var response = await conn.QueryAsync<GetUserResponse>(query, new { chatId });
            return response;
        }

        public async Task<GetUserResponse> GetUserById(SqlConnection conn, int userId)
        {
            var query = "SELECT Id, ChatId, IsBanned, IsModerator, Email, Metadata FROM Users WHERE Id = @userId";
            var response = await conn.QueryFirstOrDefaultAsync<GetUserResponse>(query, new { userId });
            return response;
        }

        public async Task<int> CreateUser(SqlConnection conn, CreateUserRequest request)
        {
            var query = @"INSERT INTO Users(ChatId, IsBanned, IsModerator, Email, Metadata)
                          OUTPUT INSERTED.Id as int
                          VALUES (@chatId, @isBanned, @isModerator, @email, @metadata)";

            var response = await conn.QuerySingleAsync<int>(query, request);
            return response;
        }

        public async Task<int> CreateUser(SqlConnection conn, SqlTransaction tran, CreateUserRequest request)
        {
            var query = @"INSERT INTO Users(ChatId, IsBanned, IsModerator, Email, Metadata)
                          OUTPUT INSERTED.Id as int
                          VALUES (@chatId, @isBanned, @isModerator, @email, @metadata)";

            var response = await conn.QuerySingleAsync<int>(query, request, tran);
            return response;
        }

        public async Task<int> SetUserIsBannedStatus(SqlConnection conn, int userId, bool isBanned)
        {
            int count = await conn.ExecuteAsync("UPDATE Users SET IsBanned = @isBanned WHERE Id = @userId", new { isBanned, userId });
            return count;
        }

        public async Task DeleteAllChatUsers(SqlConnection conn, SqlTransaction tran, int chatId)
        {
            await conn.ExecuteAsync("DELETE FROM Users WHERE ChatId = @chatId", new { chatId }, tran);
        }

        public async Task<int> UpdateUser(SqlConnection conn, UpdateUserRequest request)
        {
            int count = await conn.ExecuteAsync("UPDATE Users SET Metadata = @metadata, IsModerator = @isModerator WHERE Id = @id", request);
            return count;
        }
    }
}