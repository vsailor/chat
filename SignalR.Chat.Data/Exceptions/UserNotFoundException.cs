﻿using System.Net;
using SignalR.Chat.Common;

namespace SignalR.Chat.Data.Exceptions
{
    public class UserNotFoundException : BaseException
    {
        public UserNotFoundException(int id) : base($"User with id <{id}> not found", ErrorCodes.UserNotFound)
        {
        }

        public UserNotFoundException(string email, HttpStatusCode statusCode) 
            : base($"User with email <{email}> not found", ErrorCodes.UserNotFound, statusCode)
        {
        }
    }
}