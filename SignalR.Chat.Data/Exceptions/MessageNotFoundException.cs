﻿using SignalR.Chat.Common;

namespace SignalR.Chat.Data.Exceptions
{
    public class MessageNotFoundException : BaseException
    {
        public MessageNotFoundException(int messageId) : base($"Message with id <{messageId}> not found", ErrorCodes.MessageNotFound)
        {
        }
    }
}