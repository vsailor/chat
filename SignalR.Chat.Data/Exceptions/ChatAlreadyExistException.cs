﻿using SignalR.Chat.Common;

namespace SignalR.Chat.Data.Exceptions
{
    public class ChatAlreadyExistException : BaseException
    {
        public ChatAlreadyExistException(string name) : base($"Chat with name <{name}> already exists", ErrorCodes.ChatAlreadyExist)
        {
        }
    }
}