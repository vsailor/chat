﻿using SignalR.Chat.Common;

namespace SignalR.Chat.Data.Exceptions
{
    public class MessageAlreadyPublishedException : BaseException
    {
        public MessageAlreadyPublishedException(int messageId) 
            : base($"Message <{messageId}> already published", ErrorCodes.MessageAlreadyPublished)
        {
        }
    }
}