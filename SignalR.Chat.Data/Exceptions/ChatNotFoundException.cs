﻿using SignalR.Chat.Common;

namespace SignalR.Chat.Data.Exceptions
{
    public class ChatNotFoundException : BaseException
    {
        public ChatNotFoundException(int id) : base($"Chat with id <{id}> not found", ErrorCodes.ChatNotFound)
        {
        }
    }
}