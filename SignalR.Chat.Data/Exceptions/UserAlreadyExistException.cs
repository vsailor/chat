﻿using SignalR.Chat.Common;

namespace SignalR.Chat.Data.Exceptions
{
    public class UserAlreadyExistException : BaseException
    {
        public UserAlreadyExistException(string email) 
            : base($"User with email <{email}> already exists", ErrorCodes.UserAlreadyExist)
        {
        }
    }
}