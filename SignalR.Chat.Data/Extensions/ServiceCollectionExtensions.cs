﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SignalR.Chat.Data.Repositories;
using SignalR.Chat.Data.Repositories.Abstracts;
using SignalR.Chat.Data.Services;
using SignalR.Chat.Data.Services.Abstracts;

namespace SignalR.Chat.Data.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void RegisterDataDependencies(this IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddTransient<IChatRepository, ChatRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IChatDataService, ChatDataService>();
            services.AddTransient<IUserDataService, UserDataService>();
        }
    }
}