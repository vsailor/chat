﻿using System;
using System.Net;

namespace SignalR.Chat.Common
{
    public class BaseException : Exception
    {
        public BaseException(string message, string errorCode, HttpStatusCode statusCode = HttpStatusCode.BadRequest) : base(message)
        {
            ErrorCode = errorCode;
            StatusCode = statusCode;
        }

        public string ErrorCode { get; }

        public HttpStatusCode StatusCode { get; }
    }
}