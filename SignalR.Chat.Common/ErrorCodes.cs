﻿namespace SignalR.Chat.Common
{
    public static class ErrorCodes
    {
        public const string ChatAlreadyExist = "CHAT_ALREADY_EXISTS";
        public const string ChatNotFound = "CHAT_NOT_FOUND";
        public const string MessageNotFound = "MESSAGE_NOT_FOUND";
        public const string ChatIdIsRequired = "CHAT_ID_IS_REQUIRED";
        public const string EmailIsRequired = "EMAIL_IS_REQUIRED";
        public const string EmailHasInvalidFormat = "EMAIL_HAS_INVALID_FORMAT";

        public const string HashIdConversionError = "HASH_ID_CONVERSION_ERROR";

        public const string UserAlreadyExist = "USER_ALREADY_EXISTS";
        public const string UserNotFound = "USER_NOT_FOUND";
        public const string UserAlreadyBanned = "USER_ALREADY_BANNED";
        public const string UserBanned = "USER_BANNED";

        public const string AuthorizationError = "AUTHORIZATION_ERROR";
        public const string PermissionDenied = "PERMISSION_DENIED";

        public const string UnknownError = "UNKNOWN_ERROR";

        public const string ChatNameShouldBeFilled = "CHAT_NAME_SHOULD_BE_FILLED";
        public const string ChatNameValidationError = "CHAT_NAME_VALIDATION_ERROR";
        public const string MaxMessagesShouldBeFilled = "MAX_MESSAGES_SHOULD_BE_FILLED";
        public const string MaxMessagesShouldBeGreaterThan0 = "MAX_MESSAGES_SHOULD_BE_GREATER_THAN_0";
        public const string MaxMessagesShouldBeLessThan1000 = "MAX_MESSAGES_SHOULD_BE_LESS_THAN_1000";
        public const string MessageShouldNotBeNull = "MESSAGE_SHOULD_NOT_BE_NULL";
        public const string MessageLengthValidationError = "MESSAGE_LENGTH_VALIDATION_ERROR";
        public const string MessageAlreadyPublished = "MESSAGE_ALREADY_PUBLISHED";
    }
}