﻿namespace SignalR.Chat.Common.Options
{
    public class AppConfigOptions
    {
        public string DbConnectionString { get; set; }

        public bool SwaggerEnabled { get; set; }

        public bool ErrorLogViewEnabled { get; set; }

        public bool UseAzureSignalR { get; set; }
    }
}