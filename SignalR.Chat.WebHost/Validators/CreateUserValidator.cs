﻿using FluentValidation;
using SignalR.Chat.Common;
using SignalR.Chat.WebHost.Models.Requests;

namespace SignalR.Chat.WebHost.Validators
{
    public class CreateUserValidator : AbstractValidator<CreateUserRequestModel>
    {
        public CreateUserValidator()
        {
            RuleFor(x => x.ChatId)
                .NotNull()
                .WithMessage(ErrorCodes.ChatIdIsRequired);

            RuleFor(x => x.Email)
                .NotNull()
                .WithMessage(ErrorCodes.EmailIsRequired)
                .Matches(x => Constants.EmailRegularExpression)
                .WithMessage(ErrorCodes.EmailHasInvalidFormat);
        }
    }
}