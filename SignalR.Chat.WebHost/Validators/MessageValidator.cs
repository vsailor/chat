﻿using FluentValidation;
using SignalR.Chat.Common;
using SignalR.Chat.WebHost.Models.Requests;

namespace SignalR.Chat.WebHost.Validators
{
    public class MessageValidator : AbstractValidator<AddMessageRequestModel>
    {
        public MessageValidator()
        {
            RuleFor(x => x.Body)
                .NotNull()
                .WithMessage(ErrorCodes.MessageShouldNotBeNull)
                .Length(1, 2000)
                .WithMessage(ErrorCodes.MessageLengthValidationError);
        }
    }
}