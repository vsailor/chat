﻿using System.Linq;
using FluentValidation;
using SignalR.Chat.Common;
using SignalR.Chat.Models.Requests;

namespace SignalR.Chat.WebHost.Validators
{
    public class CreateChatValidator : AbstractValidator<CreateChatRequest>
    {
        public CreateChatValidator()
        {
            RuleFor(x => x.ChatName)
                .NotNull()
                .WithMessage(ErrorCodes.ChatNameShouldBeFilled)
                .Length(5, 500)
                .WithMessage(ErrorCodes.ChatNameValidationError);

            RuleFor(x => x.MaxMessages)
                .NotNull()
                .WithMessage(ErrorCodes.MaxMessagesShouldBeFilled)
                .GreaterThan(0)
                .WithMessage(ErrorCodes.MaxMessagesShouldBeGreaterThan0)
                .LessThan(1000)
                .WithMessage(ErrorCodes.MaxMessagesShouldBeLessThan1000);

            RuleFor(x => x.AutoApproveMessages)
                .NotNull();

            RuleFor(x => x.ModeratorEmails)
                .NotNull()
                .Must(x => !x.GroupBy(d => d).Any(g => g.Count() > 1))
                .WithMessage("Emails should not have duplications");
        }
    }
}