﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using SignalR.Chat.Authorization;
using SignalR.Chat.Authorization.Abstracts;
using SignalR.Chat.Authorization.Exceptions;
using SignalR.Chat.Authorization.Extensions;
using SignalR.Chat.Common;
using SignalR.Chat.Data.Services.Abstracts;
using SignalR.Chat.Models.Requests;
using SignalR.Chat.WebHost.Extensions;
using SignalR.Chat.WebHost.Hubs;
using SignalR.Chat.WebHost.Models;
using SignalR.Chat.WebHost.Models.Requests;
using SignalR.Chat.WebHost.Models.Responses;
using SignalR.Chat.WebHost.Services.Abstracts;

namespace SignalR.Chat.WebHost.Controllers
{
    [Route("api/chats")]
    [ApiController]
    public class ChatController : ControllerBase
    {
        private readonly IChatDataService _chatDataService;
        private readonly IUserDataService _userDataService;
        private readonly IHubContext<ChatHub> _chatHubContext;
        private readonly IChatHubService _chatHubService;
        private readonly ITokenProviderService _tokenProviderService;

        public ChatController(
            IChatDataService chatDataService,
            IUserDataService userDataService,
            IChatHubService chatHubService,
            ITokenProviderService tokenProviderService,
            IHubContext<ChatHub> chatHubContext)
        {
            _chatDataService = chatDataService;
            _userDataService = userDataService;
            _chatHubService = chatHubService;
            _tokenProviderService = tokenProviderService;
            _chatHubContext = chatHubContext;
        }

        [Authorize(Roles = Roles.AdminOrModerator)]
        [HttpPost]
        public async Task<IActionResult> CreateChat([FromBody] CreateChatRequest request)
        {
            var chatId = await _chatDataService.CreateChat(request);
            return Ok(chatId.Encode());
        }

        [Authorize(Roles = Roles.AdminOrModerator)]
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(GetChatResponseModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetChatById(string id)
        {
            var chat = await _chatDataService.GetChatById(id.Decode());
            if (chat == null)
                return NotFound();

            return Ok(new GetChatResponseModel(id, chat.ChatName, chat.MaxMessages, chat.AutoApproveMessages, chat.ModeratorEmails));
        }

        [Authorize(Roles = Roles.All)]
        [HttpGet]
        public async Task<IEnumerable<GetAllChatsResponseModel>> GetAllChats()
        {
            var chats = await _chatDataService.GetAllChats();
            return chats.Select(c =>
                new GetAllChatsResponseModel(c.Id.Encode(), c.ChatName, c.MaxMessages, c.AutoApproveMessages));
        }

        [Authorize(Roles = Roles.Admin)]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateChat(string id, [FromBody] UpdateChatRequestModel request)
        {
            await _chatDataService.UpdateChat(new UpdateChatRequest
            {
                Id = id.Decode(),
                MaxMessages = request.MaxMessages,
                ChatName = request.ChatName,
                AutoApproveMessages = request.AutoApproveMessages,
                ModeratorEmails = request.ModeratorEmails
            });

            return Ok();
        }

        [Authorize(Roles = Roles.Admin)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteChat(string id)
        {
            var chatId = id.Decode();
            await _chatDataService.DeleteChat(chatId);
            _chatHubService.RemoveAllClientsFromChat(id);
            return Ok();
        }

        [Authorize(Roles = Roles.All)]
        [HttpGet("{id}/messages")]
        public async Task<IEnumerable<GetMessageResponseModel>> GetAllMessages(string id, double? startDate)
        {
            DateTime? startTime = null;
            if(startDate.HasValue)
            {
                DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                startTime = origin.AddMilliseconds(startDate.Value);
            }
            

            bool onlyPublished = User.IsInRole(Roles.User);
            var request = new GetAllMessagesRequest
            {
                ChatId = id.Decode(),
                OnlyPublished = onlyPublished,
                StartDate = startTime
            };

            if (!User.IsInRole(Roles.Admin))
                request.UserId = User.Identity.GetUser().UserId.Decode();

            var messages = await _chatDataService.GetAllMessages(request);

            

            return messages.Select(c =>
                new GetMessageResponseModel(
                    c.Id.Encode(), 
                    c.Body, 
                    c.CreatedOn, 
                    c.CreatedBy.Encode(), 
                    c.IsDeleted, 
                    c.Metadata.HideMetadata(JsonConvert.SerializeObject(new { name = c.Email })), 
                    c.IsBanned, 
                    c.IsPublished, 
                    c.IsModerator));
        }

        [Authorize(Roles = Roles.AdminOrModerator)]
        [HttpPost("{id}/messages")]
        public async Task<IActionResult> AddMessage(string id, [FromBody] AddMessageRequestModel request)
        {
            var user = User.Identity.GetUser();

            var messageId = await _chatDataService.AddMessage(
                new CreateMessageRequest(user.UserId.Decode(), request.Body, id.Decode()));
            var message = await _chatDataService.GetMessageById(messageId);

            var response = new GetMessageResponseModel(
                message.Id.Encode(),
                message.Body,
                message.CreatedOn,
                user.UserId,
                message.IsDeleted,
                message.Metadata,
                user.IsBanned,
                message.IsPublished,
                message.IsModerator);

            await _chatHubContext.Clients.Group(id).SendAsync(ChatHubClientMethods.OnMessageReceived, response);

            return Ok(response);
        }

        [Authorize(Roles = Roles.AdminOrModerator)]
        [HttpPost("messages/publish")]
        public async Task<IActionResult> PublishMessage([FromBody] PublishMessageRequestModel request)
        {
            var messageId = request.Id.Decode();
            await _chatDataService.PublishMessage(messageId);
            var message = await _chatDataService.GetMessageById(messageId);

            var notificationModel = new GetMessageResponseModel(
                message.Id.Encode(),
                message.Body,
                message.CreatedOn,
                message.CreatedBy.Encode(),
                message.IsDeleted,
                message.Metadata,
                message.IsBanned,
                message.IsPublished,
                message.IsModerator);

            await _chatHubContext.Clients.Group(message.ChatId.Encode()).SendAsync(ChatHubClientMethods.OnMessageReceived, notificationModel);

            return Ok();
        }

        [Authorize(Roles = Roles.All)]
        [HttpPost("delete-message")]
        public async Task<IActionResult> DeleteMessage([FromBody] DeleteMessageRequestModel request)
        {
            var message = await _chatDataService.GetMessageById(request.Id.Decode());

            var user = User.Identity.GetUser();
            if (User.IsInRole(Roles.User) && message.CreatedBy.Encode() != user.UserId)
                throw new AuthorizationException("Can't delete others messages", ErrorCodes.PermissionDenied, HttpStatusCode.Forbidden);

            await _chatDataService.DeleteMessage(request.Id.Decode());
            await _chatHubService.SendMessageDeletedNotification(message.ChatId.Encode(), request.Id);
            return Ok();
        }

        [Authorize(Roles = Roles.Admin)]
        [HttpPost("delete-messages")]
        public async Task<IActionResult> DeleteAllMessages([FromBody] DeleteMessagesRequestModel request)
        {
            await _chatDataService.DeleteMessages(request.ChatId.Decode());
            await _chatHubService.SendAllMessagesDeletedNotification(request.ChatId.Decode());
            return Ok();
        }

        [Authorize(Roles = Roles.AdminOrModerator)]
        [HttpGet("{id}/users")]
        public async Task<IEnumerable<GetUserResponseModel>> GetUsers(string id)
        {
            var users = await _userDataService.GetAllUsers(id.Decode());

            return users.Select(u =>
                new GetUserResponseModel
                {
                    Id = u.Id.Encode(),
                    Email = u.Email,
                    IsModerator = u.IsModerator,
                    IsBanned = u.IsBanned,
                    Metadata = u.Metadata
                });
        }
    }
}