﻿using Microsoft.AspNetCore.Mvc;

namespace SignalR.Chat.WebHost.Controllers
{
    [Route("")]
    [ApiController]
    public class HomeController : ControllerBase
    {

        [HttpGet]
        public IActionResult Get()
        {
            return File("~/index.html", "text/html");
        }
    }
}