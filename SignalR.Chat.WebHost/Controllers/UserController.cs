﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SignalR.Chat.Authorization;
using SignalR.Chat.Authorization.Exceptions;
using SignalR.Chat.Authorization.Extensions;
using SignalR.Chat.Common;
using SignalR.Chat.Data.Exceptions;
using SignalR.Chat.Data.Services.Abstracts;
using SignalR.Chat.Models.Requests;
using SignalR.Chat.WebHost.Exceptions;
using SignalR.Chat.WebHost.Extensions;
using SignalR.Chat.WebHost.Models;
using SignalR.Chat.WebHost.Models.Requests;
using SignalR.Chat.WebHost.Models.Responses;
using SignalR.Chat.WebHost.Services.Abstracts;

namespace SignalR.Chat.WebHost.Controllers
{
    [Route("api/users")]
    [Authorize(Roles = Roles.AdminOrModerator)]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserDataService _userDataService;
        private readonly IChatDataService _chatDataService;
        private readonly IChatHubService _chatHubService;

        public UserController(
            IUserDataService userDataService, 
            IChatDataService chatDataService, 
            IChatHubService chatHubService)
        {
            _userDataService = userDataService;
            _chatDataService = chatDataService;
            _chatHubService = chatHubService;
        }

        [HttpGet("{id}")]
        public async Task<GetUserResponseModel> GetUserById(string id)
        {
            var user = await _userDataService.GetUserById(id.Decode());
            return new GetUserResponseModel
            {
                Id = id,
                Email = user.Email,
                IsModerator = user.IsModerator,
                IsBanned = user.IsBanned,
                Metadata = user.Metadata
            };
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser(CreateUserRequestModel request)
        {
            if (request.IsModerator && !User.IsInRole(Roles.Admin))
                throw new AuthorizationException("Moderator can be created only by admin", ErrorCodes.PermissionDenied, HttpStatusCode.Forbidden);

            await _userDataService.CreateUser(new CreateUserRequest
            {
                ChatId = request.ChatId.Decode(),
                Email = request.Email,
                IsBanned = false,
                IsModerator = request.IsModerator,
                Metadata = request.Metadata
            });

            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUser(string id, [FromBody] UpdateUserRequestModel request)
        {
            var userId = id.Decode();
            var user = await _userDataService.GetUserById(userId);
            if (user == null)
                throw new UserNotFoundException(userId);

            if (User.Identity.GetUser().UserId == id && request.IsModerator.HasValue)
                throw new AuthorizationException("Moderator can be updated or created only by admin", ErrorCodes.PermissionDenied, HttpStatusCode.Forbidden);

            if ((request.IsModerator.HasValue || user.IsModerator) && !User.IsInRole(Roles.Admin))
                throw new AuthorizationException("Moderator can be updated or created only by admin", ErrorCodes.PermissionDenied, HttpStatusCode.Forbidden);

            await _userDataService.UpdateUser(new UpdateUserRequest
            {
                Id = userId,
                IsModerator = request.IsModerator ?? user.IsModerator,
                Metadata = request.Metadata
            });

            return Ok();
        }

        [HttpPost("ban-user")]
        public async Task<IActionResult> BanUser([FromBody] BanUserRequestModel request)
        {
            var userId = request.Id.Decode();
            var user = await _userDataService.GetUserById(userId);
            if (user == null)
                throw new UserNotFoundException(userId);

            if (user.IsBanned)
                throw new UserAlreadyBannedException(request.Id);

            if (user.IsModerator && !User.IsInRole(Roles.Admin))
                throw new AuthorizationException("Can't ban moderator", ErrorCodes.PermissionDenied, HttpStatusCode.Forbidden);

            if (request.Id == User.Identity.GetUser().UserId)
                throw new AuthorizationException("Can't ban myself", ErrorCodes.PermissionDenied, HttpStatusCode.Forbidden);

            await _userDataService.SetUserIsBannedStatus(userId, true);

            await _chatHubService.SendUserEventNotification(NotificationEventType.UserBanned, user.ChatId, user);
            _chatHubService.CloseConnections(user.Id);

            var deletedMessages = await _chatDataService.DeleteAllMessages(userId);
            await _chatHubService.SendMessagesDeletedNotification(user.ChatId, deletedMessages);
            return Ok();
        }

        [HttpPost("unban-user")]
        public async Task<IActionResult> UnbanUser([FromBody] BanUserRequestModel request)
        {
            var user = await _userDataService.GetUserById(request.Id.Decode());

            if (user.IsModerator && !User.IsInRole(Roles.Admin))
                throw new AuthorizationException("Can't unban moderator", ErrorCodes.PermissionDenied, HttpStatusCode.Forbidden);

            if (request.Id == User.Identity.GetUser().UserId)
                throw new AuthorizationException("Can't unban myself", ErrorCodes.PermissionDenied, HttpStatusCode.Forbidden);

            await _userDataService.SetUserIsBannedStatus(request.Id.Decode(), false);

            await _chatHubService.SendUserEventNotification(NotificationEventType.UserUnbanned, user.ChatId, user);
            return Ok();
        }
    }
}