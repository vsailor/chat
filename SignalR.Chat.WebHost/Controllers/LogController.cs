﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SignalR.Chat.Authorization.Models;
using SignalR.Chat.WebHost.Exceptions;
using SignalR.Chat.WebHost.Logger.Abstracts;
using SignalR.Chat.WebHost.Models.Requests;

namespace SignalR.Chat.WebHost.Controllers
{
    [Route("api/logs")]
    [ApiController]
    public class LogController : ControllerBase
    {
        private readonly IChatLogger _logger;

        public LogController(IChatLogger logger)
        {
            _logger = logger;
        }

        [HttpPost]
        [Route("error")]
        public async Task<IActionResult> SendError([FromBody] SendErrorRequestModel request)
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var data = JsonConvert.SerializeObject(request, Formatting.Indented);
            await _logger.LogAsync(new ClientSideException(data), HttpContext, claimsIdentity.FindFirst(ClaimNames.Email)?.Value);
            return Ok();
        }

        [HttpPost]
        [Route("info")]
        public async Task<IActionResult> SendLog([FromBody] SendLogRequestModel request)
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var data = JsonConvert.SerializeObject(request, Formatting.Indented);
            await _logger.LogAsync(new ClientSideException(data), HttpContext, claimsIdentity.FindFirst(ClaimNames.Email)?.Value);
            return Ok();
        }
    }
}