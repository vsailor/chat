﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Microsoft.Net.Http.Headers;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using SignalR.Chat.Authorization;
using SignalR.Chat.Authorization.Abstracts;
using SignalR.Chat.Authorization.Exceptions;
using SignalR.Chat.Authorization.Extensions;
using SignalR.Chat.Authorization.Models;
using SignalR.Chat.Common;
using SignalR.Chat.Data.Services.Abstracts;
using SignalR.Chat.WebHost.Extensions;
using SignalR.Chat.WebHost.Models.Responses;

namespace SignalR.Chat.WebHost.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly ITokenProviderService _tokenProviderService;
        private readonly IUserDataService _userDataService;

        public AuthController(
            IUserDataService userDataService, 
            ITokenProviderService tokenProviderService)
        {
            _userDataService = userDataService;
            _tokenProviderService = tokenProviderService;
        }

        [HttpPost("get-token")]
        public async Task<GetTokenResponseModel> GetToken()
        {
            var response = await Task.Run(() => _tokenProviderService.GenerateToken(
                new ApplicationUser
                {
                    Role = Roles.Admin
                }));

            return new GetTokenResponseModel(response);
        }

        [HttpPost("refresh-token")]
        public async Task<GetTokenResponseModel> RefreshToken()
        {
            HttpContext.Request.Headers.TryGetValue(HeaderNames.Authorization, out StringValues headers);

            var token = headers.FirstOrDefault();
            if (token == null)
                throw new AuthorizationException("Token required", ErrorCodes.AuthorizationError, HttpStatusCode.Unauthorized);

            token = token.Split(" ").LastOrDefault();
            string newToken;

            var identityUser = User.Identity.GetUser();
            if (identityUser.Role == Roles.Admin)
            {
                newToken = _tokenProviderService.RefreshToken(token);
                return new GetTokenResponseModel(newToken);
            }

            var userId = identityUser.UserId.Decode();
            var user = await _userDataService.GetUserById(userId);
            string role = user.IsModerator ? Roles.Moderator : Roles.User;

            newToken = _tokenProviderService.GenerateToken(
                new ApplicationUser(user.Id.Encode(), user.Metadata, user.Email, user.ChatId.Encode(), role, user.IsBanned));

            return new GetTokenResponseModel(newToken);
        }
    }
}