﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using StackExchange.Exceptional;
using SignalR.Chat.Authorization;
using SignalR.Chat.Authorization.Extensions;
using SignalR.Chat.Common;
using SignalR.Chat.Data.Services.Abstracts;
using SignalR.Chat.Models.Requests;
using SignalR.Chat.WebHost.Extensions;
using SignalR.Chat.WebHost.Logger.Abstracts;
using SignalR.Chat.WebHost.Models;
using SignalR.Chat.WebHost.Models.Requests;
using SignalR.Chat.WebHost.Models.Responses;
using SignalR.Chat.WebHost.Services.Abstracts;
using SignalR.Chat.WebHost.Validators;

namespace SignalR.Chat.WebHost.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        private readonly IChatLogger _logger;
        private readonly IChatDataService _chatDataService;
        private readonly IUserDataService _userDataService;
        private readonly IChatHubService _chatHubService;

        public ChatHub(IChatDataService chatDataService, IUserDataService userDataService, IChatHubService chatHubService, IChatLogger logger)
        {
            _chatDataService = chatDataService;
            _userDataService = userDataService;
            _chatHubService = chatHubService;
            _logger = logger;
        }

        public async Task Send(string message)
        {
            var user = Context.User.Identity.GetUser();
            if (user.IsBanned)
            {
                await _chatHubService.SendMessageErrorNotification(Context.ConnectionId, ErrorCodes.UserBanned);
                return;
            }

            var validator = new MessageValidator();
            var validationResult = await validator.ValidateAsync(new AddMessageRequestModel(message));
            if (!validationResult.IsValid)
            {
                await _chatHubService.SendMessageErrorNotification(Context.ConnectionId, validationResult.Errors.First().ErrorMessage);
                return;
            }

            var messageId = await _chatDataService.AddMessage(
                new CreateMessageRequest(user.UserId.Decode(), message, user.ChatId.Decode()));

            if (user.Role != Roles.User)
                await _chatDataService.PublishMessage(messageId);

            var newMessage = await _chatDataService.GetMessageById(messageId);
            var newMessageNotification = new GetMessageResponseModel(
                newMessage.Id.Encode(),
                newMessage.Body,
                newMessage.CreatedOn,
                user.UserId,
                newMessage.IsDeleted,
                newMessage.Metadata.HideMetadata(),
                user.IsBanned,
                newMessage.IsPublished,
                newMessage.IsModerator);

            if (newMessage.IsPublished)
            {
                await Clients.Group(user.ChatId)
                    .SendAsync(ChatHubClientMethods.OnMessageReceived, newMessageNotification);
            }
            else
            {
                await _chatHubService.SendMessageToModerators(user.ChatId, newMessageNotification);
                await _chatHubService.SendMessageToUser(user.UserId, newMessageNotification);
            }

            var log = $"Received message: <{newMessage.Id.Encode()}> from user <{user.UserId}> in chat <{user.ChatId}>";
            await _logger.LogAsync(new Exception(log), null, log);
        }

        public override async Task OnConnectedAsync()
        {
            var identityUser = Context.User.Identity.GetUser();
            var user = await _userDataService.GetUserById(identityUser.UserId.Decode());
            var log = string.Empty;
            if (user == null)
            {
                await Clients.Caller.SendAsync(ChatHubClientMethods.OnMessageReceived, new { error = ErrorCodes.UserNotFound });
                Context.Abort();
                log = $"Token of user <{identityUser.UserId}> is not valid";
                await _logger.LogAsync(new Exception(log), null, log);
                return;
            }

            if (identityUser.IsBanned != user.IsBanned)
            {
                await Clients.Caller.SendAsync(ChatHubClientMethods.OnMessageReceived, new { error = ErrorCodes.UserBanned });
                Context.Abort();
                log = $"Token of user <{identityUser.UserId}> is not valid";
                await _logger.LogAsync(new Exception(log), null, log);
                return;
            }

            _chatHubService.AddConnection(identityUser, Context);
            await Groups.AddToGroupAsync(Context.ConnectionId, identityUser.ChatId);
            await _chatHubService.SendUserEventNotification(NotificationEventType.UserJoined, user.ChatId, user);

            log = $"User <{identityUser.Email}> joined chat <{identityUser.ChatId}>";
            await _logger.LogAsync(new Exception(log), null, log);
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var identityUser = Context.User.Identity.GetUser();
            var user = await _userDataService.GetUserById(identityUser.UserId.Decode());
            if (user == null)
                return;

            _chatHubService.CloseConnection(user.Id, Context.ConnectionId);

            await _chatHubService.SendUserEventNotification(NotificationEventType.UserLeft, user.ChatId, user);
            var log = $"User <{identityUser.Email}> left chat <{identityUser.ChatId}>";
            await _logger.LogAsync(new Exception(log), null, log);
            await base.OnDisconnectedAsync(exception);
        }
    }
}