﻿using System;

namespace SignalR.Chat.WebHost.Exceptions
{
    public class ClientSideException : Exception
    {
        public ClientSideException(string data) : base(data)
        {
        }
    }
}