﻿using SignalR.Chat.Common;

namespace SignalR.Chat.WebHost.Exceptions
{
    public class HashIdConversionException : BaseException
    {
        public HashIdConversionException(string value) : base($"Can't convert <{value}>", ErrorCodes.HashIdConversionError)
        {
        }
    }
}