﻿using SignalR.Chat.Common;

namespace SignalR.Chat.WebHost.Exceptions
{
    public class UserAlreadyBannedException : BaseException
    {
        public UserAlreadyBannedException(string userId) : base($"User {userId} already banned", ErrorCodes.UserAlreadyBanned)
        {
        }
    }
}