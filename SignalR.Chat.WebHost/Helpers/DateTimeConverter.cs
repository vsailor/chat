﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SignalR.Chat.WebHost.Helpers
{
    public class DateTimeConverter : JsonConverter<DateTime>
    {
        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            return DateTime.Parse(reader.GetString());
        }

        public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
        {
            value = DateTime.SpecifyKind(value, DateTimeKind.Utc);
            writer.WriteStringValue(value.ToUniversalTime());
        }
    }
}