﻿namespace SignalR.Chat.WebHost.Models
{
    public enum NotificationEventType
    {
        UserBanned,
        UserUnbanned,
        UserJoined,
        UserLeft,
        MessageDeleted,
        MessagesDeleted,
        AllMessagesDeleted,
        SendMessageError
    }
}