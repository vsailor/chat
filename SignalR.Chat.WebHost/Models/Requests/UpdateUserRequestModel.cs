﻿namespace SignalR.Chat.WebHost.Models.Requests
{
    public class UpdateUserRequestModel
    {
        public bool? IsModerator { get; set; }

        public string Metadata { get; set; }
    }
}