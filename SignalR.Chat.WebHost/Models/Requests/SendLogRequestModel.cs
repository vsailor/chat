﻿namespace SignalR.Chat.WebHost.Models.Requests
{
    public class SendLogRequestModel
    {
        public string Method { get; set; }

        public string Message { get; set; }   

        public string DeviceInfo { get; set; }
    }
}