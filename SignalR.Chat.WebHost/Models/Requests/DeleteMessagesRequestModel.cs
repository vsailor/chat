﻿namespace SignalR.Chat.WebHost.Models.Requests
{
    public class DeleteMessagesRequestModel
    {
        public string ChatId { get; set; }
    }
}