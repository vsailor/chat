﻿namespace SignalR.Chat.WebHost.Models.Requests
{
    public class SendErrorRequestModel
    {
        public string Error { get; set; }

        public int LineNumber { get; set; }

        public string Stack { get; set; }
    }
}