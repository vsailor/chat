﻿using System.Collections.Generic;

namespace SignalR.Chat.WebHost.Models.Requests
{
    public class UpdateChatRequestModel
    {
        public string ChatName { get; set; }

        public int? MaxMessages { get; set; }

        public bool? AutoApproveMessages { get; set; }

        public IEnumerable<string> ModeratorEmails { get; set; }
    }
}