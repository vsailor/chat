﻿namespace SignalR.Chat.WebHost.Models.Requests
{
    public class PublishMessageRequestModel
    {
        public string Id { get; set; }
    }
}