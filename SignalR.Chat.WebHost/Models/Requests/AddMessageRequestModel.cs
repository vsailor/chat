﻿namespace SignalR.Chat.WebHost.Models.Requests
{
    public class AddMessageRequestModel
    {
        public AddMessageRequestModel()
        {
        }

        public AddMessageRequestModel(string body)
        {
            Body = body;
        }

        public string Body { get; set; }
    }
}