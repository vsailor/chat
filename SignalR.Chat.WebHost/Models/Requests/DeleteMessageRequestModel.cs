﻿namespace SignalR.Chat.WebHost.Models.Requests
{
    public class DeleteMessageRequestModel
    {
        public string Id { get; set; }
    }
}