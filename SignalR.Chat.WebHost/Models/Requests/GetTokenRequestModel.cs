﻿namespace SignalR.Chat.WebHost.Models.Requests
{
    public class GetTokenRequestModel
    {
        public string Email { get; set; }

        public string ChatId { get; set; }
    }
}