﻿namespace SignalR.Chat.WebHost.Models.Requests
{
    public class CreateUserRequestModel
    {
        public string ChatId { get; set; }

        public string Email { get; set; }

        public bool IsModerator { get; set; }

        public string Metadata { get; set; }
    }
}