﻿namespace SignalR.Chat.WebHost.Models.Requests
{
    public class DeleteChatRequestModel
    {
        public string Id { get; set; }
    }
}