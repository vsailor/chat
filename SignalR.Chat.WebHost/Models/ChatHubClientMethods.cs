﻿namespace SignalR.Chat.WebHost.Models
{
    public static class ChatHubClientMethods
    {
        public const string OnMessageReceived = "OnMessageReceived";
        public const string OnNotificationReceived = "OnNotificationReceived";
    }
}