﻿using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR;
using SignalR.Chat.Authorization.Models;

namespace SignalR.Chat.WebHost.Models
{
    public class ChatHubConnections
    {
        public List<HubCallerContext> Contexts { get; } = new List<HubCallerContext>();

        public ApplicationUser ApplicationUser { get; }

        public ChatHubConnections(HubCallerContext newContext, ApplicationUser user)
        {
            Contexts.Add(newContext);
            ApplicationUser = user;
        }
    }
}