﻿namespace SignalR.Chat.WebHost.Models.Responses
{
    public class GetAllChatsResponseModel
    {
        public GetAllChatsResponseModel(string id, string chatName, int maxMessages, bool autoApproveMessages)
        {
            Id = id;
            ChatName = chatName;
            MaxMessages = maxMessages;
            AutoApproveMessages = autoApproveMessages;
        }

        public string Id { get; set; }

        public string ChatName { get; set; }

        public int MaxMessages { get; set; }

        public bool AutoApproveMessages { get; set; }
    }
}