﻿namespace SignalR.Chat.WebHost.Models.Responses
{
    public class ChatNotificationResponse
    {
        public NotificationEventType EventType { get; set; }

        public dynamic Data { get; set; }
    }
}