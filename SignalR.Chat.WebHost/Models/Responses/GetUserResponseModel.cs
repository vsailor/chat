﻿namespace SignalR.Chat.WebHost.Models.Responses
{
    public class GetUserResponseModel
    {
        public string Id { get; set; }

        public bool IsBanned { get; set; }

        public bool IsModerator { get; set; }

        public string Email { get; set; }

        public string Metadata { get; set; }
    }
}