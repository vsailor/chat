﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace SignalR.Chat.WebHost.Models.Responses
{
    public class GetMessageResponseModel
    {
        public GetMessageResponseModel(string id, string body, DateTime createdOn, string createdBy, bool isDeleted, string metadata, bool isBanned, bool isPublished, bool isModerator)
        {
            Id = id;
            Body = body;
            CreatedOn = createdOn;
            CreatedBy = createdBy;
            IsDeleted = isDeleted;
            _metadata = metadata;
            IsBanned = isBanned;
            IsPublished = isPublished;
            IsModerator = isModerator;
        }

        private string _metadata;

        public string Id { get; set; }

        public string Body { get; set; }

        public DateTime CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsPublished { get; set; }

        public Dictionary<string, object> Metadata { 
            get
            {
                return !string.IsNullOrWhiteSpace(_metadata) ? JsonConvert.DeserializeObject<Dictionary<string, object>>(_metadata) : null;
            }
        }

        public bool IsBanned { get; set; }

        public bool IsModerator { get; set; }
    }
}