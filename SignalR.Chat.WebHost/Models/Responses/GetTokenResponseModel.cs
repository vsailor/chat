﻿namespace SignalR.Chat.WebHost.Models.Responses
{
    public class GetTokenResponseModel
    {
        public GetTokenResponseModel(string token)
        {
            Token = token;
        }

        public string Token { get; set; }
    }
}