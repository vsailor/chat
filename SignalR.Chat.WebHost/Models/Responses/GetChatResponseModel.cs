﻿using System.Collections.Generic;

namespace SignalR.Chat.WebHost.Models.Responses
{
    public class GetChatResponseModel
    {
        public GetChatResponseModel(string id, string chatName, int maxMessages, bool autoApproveMessages, IEnumerable<string> moderatorEmails)
        {
            Id = id;
            ChatName = chatName;
            MaxMessages = maxMessages;
            AutoApproveMessages = autoApproveMessages;
            ModeratorEmails = moderatorEmails;
        }

        public string Id { get; set; }

        public string ChatName { get; set; }

        public int MaxMessages { get; set; }

        public bool AutoApproveMessages { get; set; }

        public IEnumerable<string> ModeratorEmails { get; set; } 
    }
}