﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using SignalR.Chat.Authorization.Models;
using SignalR.Chat.Models.Responses;
using SignalR.Chat.WebHost.Models;
using SignalR.Chat.WebHost.Models.Responses;

namespace SignalR.Chat.WebHost.Services.Abstracts
{
    public interface IChatHubService
    {
        Task SendUserEventNotification(NotificationEventType eventType, int chatId, GetUserResponse initiator);
        Task SendMessageDeletedNotification(string chatId, string messageId);
        Task SendMessagesDeletedNotification(int chatId, IEnumerable<int> messagesIds);

        Task SendAllMessagesDeletedNotification(int chatId);

        Task SendMessageErrorNotification(string connectionId, string errorCode);
        void AddConnection(ApplicationUser user, HubCallerContext context);
        void CloseConnection(int userId, string connectionId);
        void CloseConnections(int userId);
        void RemoveAllClientsFromChat(string chatId);
        Task SendMessageToModerators(string chatId, GetMessageResponseModel newMessageNotification);
        Task SendMessageToUser(string userId, GetMessageResponseModel newMessageNotification);
    }
}