﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using SignalR.Chat.Authorization;
using SignalR.Chat.Authorization.Extensions;
using SignalR.Chat.Authorization.Models;
using SignalR.Chat.Models.Responses;
using SignalR.Chat.WebHost.Extensions;
using SignalR.Chat.WebHost.Hubs;
using SignalR.Chat.WebHost.Models;
using SignalR.Chat.WebHost.Models.Responses;
using SignalR.Chat.WebHost.Services.Abstracts;

namespace SignalR.Chat.WebHost.Services
{
    public class ChatHubService : IChatHubService
    {
        private readonly IHubContext<ChatHub> _context;
        protected static object _lock = new object();

        private readonly ConcurrentDictionary<int, ChatHubConnections> _hubCallerContexts;

        public ChatHubService(IHubContext<ChatHub> context)
        {
            _context = context;
            _hubCallerContexts = new ConcurrentDictionary<int, ChatHubConnections>();
        }

        public async Task SendUserEventNotification(NotificationEventType eventType, int chatId, GetUserResponse initiator)
        {
            await _context.Clients.Group(chatId.Encode()).SendAsync(
                ChatHubClientMethods.OnNotificationReceived,
                new ChatNotificationResponse
                {
                    EventType = eventType,
                    Data = new GetUserResponseModel
                    {
                        Email = initiator.Email,
                        IsBanned = initiator.IsBanned,
                        Id = initiator.Id.Encode(),
                        Metadata = initiator.Metadata
                    }
                });
        }

        public async Task SendMessageDeletedNotification(string chatId, string messageId)
        {
            await _context.Clients.Group(chatId).SendAsync(
                ChatHubClientMethods.OnNotificationReceived,
                new ChatNotificationResponse
                {
                    EventType = NotificationEventType.MessageDeleted,
                    Data = messageId
                });
        }

        public async Task SendMessagesDeletedNotification(int chatId, IEnumerable<int> messagesIds)
        {
            await _context.Clients.Group(chatId.Encode()).SendAsync(
                ChatHubClientMethods.OnNotificationReceived,
                new ChatNotificationResponse
                {
                    EventType = NotificationEventType.MessagesDeleted,
                    Data = messagesIds.Select(m => m.Encode())
                });
        }

        public async Task SendAllMessagesDeletedNotification(int chatId)
        {
            await _context.Clients.Group(chatId.Encode()).SendAsync(
                ChatHubClientMethods.OnNotificationReceived,
                new ChatNotificationResponse
                {
                    EventType = NotificationEventType.AllMessagesDeleted
                });
        }

        public async Task SendMessageErrorNotification(string connectionId, string errorCode)
        {
            await _context.Clients.Client(connectionId).SendAsync(
                ChatHubClientMethods.OnNotificationReceived,
                new ChatNotificationResponse
                {
                    EventType = NotificationEventType.SendMessageError,
                    Data = errorCode
                });
        }

        public void AddConnection(ApplicationUser user, HubCallerContext context)
        {
            var userId = user.UserId.Decode();
            if (!_hubCallerContexts.TryGetValue(userId, out ChatHubConnections connections))
            {
                _hubCallerContexts.TryAdd(userId, new ChatHubConnections(context, user));
                return;
            }

            connections.Contexts.Add(context);
        }

        public void CloseConnection(int userId, string connectionId)
        {
            if (!_hubCallerContexts.TryGetValue(userId, out ChatHubConnections connections))
                return;

            lock (_lock)
            {
                var context = connections.Contexts.FirstOrDefault(c => c.ConnectionId == connectionId);
                if (context != null)
                {
                    connections.Contexts.Remove(context);
                    context.Abort();
                }
            }
        }

        public void CloseConnections(int userId)
        {
            if (!_hubCallerContexts.TryGetValue(userId, out ChatHubConnections connections))
                return;

            foreach (var context in connections.Contexts)
                context.Abort();

            _hubCallerContexts.TryRemove(userId, out ChatHubConnections _);
        }

        public void RemoveAllClientsFromChat(string chatId)
        {
            foreach (var context in _hubCallerContexts.Values)
            {
                var connections = context.Contexts.Where(c => c.User.Identity.GetUser().ChatId == chatId).ToList();
                connections.ForEach(c => c.Abort());
            }
        }

        public async Task SendMessageToModerators(string chatId, GetMessageResponseModel newMessageNotification)
        {
            var moderators =
                _hubCallerContexts.Values.Where(c => c.ApplicationUser.ChatId == chatId && c.ApplicationUser.Role != Roles.User)
                    .SelectMany(m => m.Contexts)
                    .Select(s => s.ConnectionId);

            await _context.Clients.Clients(moderators.ToList()).SendAsync(ChatHubClientMethods.OnMessageReceived, newMessageNotification);
        }

        public async Task SendMessageToUser(string userId, GetMessageResponseModel newMessageNotification)
        {
            var userConnections = _hubCallerContexts.Values.Where(c => c.ApplicationUser.UserId == userId)
                .SelectMany(m => m.Contexts)
                .Select(s => s.ConnectionId);

            await _context.Clients.Clients(userConnections.ToList()).SendAsync(ChatHubClientMethods.OnMessageReceived, newMessageNotification);
        }
    }
}