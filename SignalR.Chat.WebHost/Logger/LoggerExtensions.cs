﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SignalR.Chat.WebHost.Logger.Abstracts;

namespace SignalR.Chat.WebHost.Logger
{
    public static class LoggerExtensions
    {
        public static void ConfigureLogger(this IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddExceptional(configuration.GetSection("Exceptional"));
            services.AddTransient<IChatLogger, ChatLogger>();
        }
    }
}