﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using StackExchange.Exceptional;
using SignalR.Chat.WebHost.Logger.Abstracts;

namespace SignalR.Chat.WebHost.Logger
{
    public class ChatLogger : IChatLogger
    {
        public void Log(Exception ex, HttpContext httpContext = null, object customLogData = null)
        {
            if (customLogData != null)
            {
                ex = ex.AddLogData("Additional-Data", customLogData);
            }
            if (httpContext == null)
            {
                ex.LogNoContext();
            }
            else
            {
                ex.Log(httpContext);
            }
        }

        public async Task LogAsync(Exception ex, HttpContext httpContext = null, object customLogData = null)
        {
            if (customLogData != null)
            {
                ex = ex.AddLogData("Additional-Data", customLogData);
            }
            if (httpContext == null)
            {
                ex.LogNoContext();
            }
            else
            {
                await ex.LogAsync(httpContext);
            }
        }
    }
}