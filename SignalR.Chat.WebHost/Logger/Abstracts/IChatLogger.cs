﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace SignalR.Chat.WebHost.Logger.Abstracts
{
    public interface IChatLogger
    {
        void Log(Exception ex, HttpContext httpContext = null, object customLogData = null);
        Task LogAsync(Exception ex, HttpContext httpContext = null, object customLogData = null);
    }
}