﻿using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SignalR.Chat.WebHost.Extensions
{
    public static class MetadataExtensions
    {
        public static string HideMetadata(this string metadata, string fallBackData = null)
        {
            if (string.IsNullOrWhiteSpace(metadata))
            {
                if(!string.IsNullOrWhiteSpace(fallBackData))
                {
                    metadata = fallBackData;
                } else
                {
                    return metadata;
                }
            }
                

            try
            {
                var jobj = JObject.Parse(metadata);
                const string propertyName = "name";
                if (jobj.TryGetValue(propertyName, out var name))
                {
                    var parsedName = ParseName(name?.ToString());
                    jobj.Remove(propertyName);
                    jobj.Add(new JProperty(propertyName, parsedName));
                    return jobj.ToString(Formatting.None);
                }
            }
            catch (JsonReaderException)
            {
                return metadata;
            }

            return metadata;
        }

        private static string ParseName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return name;

            if (name.Contains('@'))
                return ParseEmail(name);

            var split = name.Split(' ');
            if (split.Length > 1)
                return $"{ split[0] } { split[1].Substring(0, 1) }.";

            return split[0];
        }

        private static string ParseEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return email;

            var split = email.Split('@');
            if (split.Length > 1)
                return $"{ split[0] }@***.{ split[1].Split('.').Last() }";

            return split[0];
        }
    }
}