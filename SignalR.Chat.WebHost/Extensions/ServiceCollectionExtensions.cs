﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SignalR.Chat.Authorization.Abstracts;
using SignalR.Chat.Authorization.Extensions;
using SignalR.Chat.Authorization.Services;
using SignalR.Chat.Common.Options;
using SignalR.Chat.Data.Extensions;
using SignalR.Chat.WebHost.Services;
using SignalR.Chat.WebHost.Services.Abstracts;

namespace SignalR.Chat.WebHost.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void RegisterDependencies(this IServiceCollection services, IConfigurationRoot configuration)
        {
            services.RegisterAuthorizationDependencies(configuration);

            services.Configure<AppConfigOptions>(configuration.GetSection("AppConfig"));
            services.AddTransient<ITokenProviderService, TokenProviderService>();
            services.AddSingleton<IChatHubService, ChatHubService>();
            services.RegisterDataDependencies(configuration);
        }
    }
}