﻿using System.Linq;
using HashidsNet;
using SignalR.Chat.WebHost.Exceptions;

namespace SignalR.Chat.WebHost.Extensions
{
    public static class HashIdExtensions
    {
        private static Hashids _hashIds = new Hashids("AaegwegWEGKWEFMlwejnkgWEGKnweg", 8);

        public static int Decode(this string encodedValue)
        {
            var decodedValues = _hashIds.Decode(encodedValue);
            if (!decodedValues.Any())
                throw new HashIdConversionException(encodedValue);

            return decodedValues.First();
        }

        public static string Encode(this int decodedValue)
        {
            if (decodedValue < 0)
                throw new HashIdConversionException(decodedValue.ToString());

            return _hashIds.Encode(decodedValue);
        }
    }
}