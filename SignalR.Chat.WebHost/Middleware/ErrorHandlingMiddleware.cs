﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using SignalR.Chat.Common;
using SignalR.Chat.WebHost.Logger.Abstracts;

namespace SignalR.Chat.WebHost.Middleware
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IChatLogger _logger;

        public ErrorHandlingMiddleware(RequestDelegate next, IChatLogger logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            HttpStatusCode code;
            string result;

            switch (exception)
            {
                case BaseException baseException:
                    code = baseException.StatusCode;
                    result = JsonConvert.SerializeObject(new { errorCode = baseException.ErrorCode });
                    break;
                default:
                    await _logger.LogAsync(exception, context);
                    code = HttpStatusCode.InternalServerError;
                    result = JsonConvert.SerializeObject(new { message = exception, errorCode = ErrorCodes.UnknownError });
                    break;
            }

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            await context.Response.WriteAsync(result);
        }
    }
}