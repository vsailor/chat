using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using StackExchange.Exceptional;
using System.Reflection;
using System.Text.Json.Serialization;
using SignalR.Chat.Authorization.Extensions;
using SignalR.Chat.Common.Options;
using SignalR.Chat.WebHost.Extensions;
using SignalR.Chat.WebHost.Helpers;
using SignalR.Chat.WebHost.Hubs;
using SignalR.Chat.WebHost.Logger;
using SignalR.Chat.WebHost.Middleware;

namespace SignalR.Chat.WebHost
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; set; }

        public AppConfigOptions appConfig { get; set; }

        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            appConfig = Configuration.GetSection("AppConfig").Get<AppConfigOptions>();


            var signalrServiceConfig = services.AddSignalR(o => { o.EnableDetailedErrors = true; });

            if(appConfig.UseAzureSignalR)
            {
                signalrServiceConfig.AddAzureSignalR();
            }

            signalrServiceConfig.AddJsonProtocol(options =>
            {
                options.PayloadSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                options.PayloadSerializerOptions.Converters.Add(new DateTimeConverter());
            });

            services.AddMvcCore(options =>
                {
                    options.Filters.Add<ValidateModelStateAttribute>();
                })
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                    options.JsonSerializerOptions.Converters.Add(new DateTimeConverter());
                })
                .AddApiExplorer()
                .AddFluentValidation(opt => { opt.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly()); });

            services.AddRouting();
            services.ConfigureLogger(Configuration);
            services.RegisterDependencies(Configuration);
            services.AddTokenProviderService(Configuration);

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificOrigin",
                    builder =>
                        builder.WithOrigins("http://localhost:4200")
                            .AllowCredentials()
                            .AllowAnyMethod()
                            .AllowAnyHeader());
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "SignalR Chat WebHost API", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            DefaultFilesOptions options = new DefaultFilesOptions();
            options.DefaultFileNames.Clear();
            options.DefaultFileNames.Add("index.html");
            app.UseDefaultFiles(options);
            app.UseStaticFiles();
            app.UseMiddleware<ErrorHandlingMiddleware>();

            if (env.IsDevelopment())
            {
                app.UseCors("AllowSpecificOrigin");
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<ChatHub>("/chat-hub");

                if (appConfig.ErrorLogViewEnabled)
                {
                    endpoints.MapGet("~/errors/log/{path?}/{subPath?}", async context =>
                        await ExceptionalMiddleware.HandleRequestAsync(context));
                }
            });

            if (appConfig.SwaggerEnabled)
            {
                app.UseSwagger();
                app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "SignalR Chat WebHost API"); });
            }
        }
    }
}