﻿namespace SignalR.Chat.Authorization.Models
{
    public class ApplicationUser
    {
        public ApplicationUser(string userId, string metadata, string email, string chatId, string role, bool isBanned)
        {
            UserId = userId;
            Metadata = metadata;
            Email = email;
            ChatId = chatId;
            Role = role;
            IsBanned = isBanned;
        }

        public ApplicationUser()
        {
        }

        public string UserId { get; set; }

        public string Metadata { get; set; }

        public string Email { get; set; }

        public string ChatId { get; set; }

        public string Role { get; set; }

        public bool IsBanned { get; set; }
    }
}