﻿namespace SignalR.Chat.Authorization.Models
{
    public static class ClaimNames
    {
        public const string UserId = "UserId";
        public const string Metadata = "Metadata";
        public const string Email = "Email";
        public const string ChatId = "ChatId";
        public const string IsBanned = "IsBanned";
    }
}