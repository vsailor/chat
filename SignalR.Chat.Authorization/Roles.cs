﻿namespace SignalR.Chat.Authorization
{
    public static class Roles
    {
        public const string Admin = "Admin";
        public const string AdminOrModerator = "Admin,Moderator";
        public const string User = "User";
        public const string Moderator = "Moderator";
        public const string All = "Admin,Moderator,User";
    }
}