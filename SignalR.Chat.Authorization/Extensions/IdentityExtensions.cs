﻿using System.Security.Claims;
using System.Security.Principal;
using SignalR.Chat.Authorization.Models;

namespace SignalR.Chat.Authorization.Extensions
{
    public static class IdentityExtensions
    {
        public static ApplicationUser GetUser(this IIdentity identity)
        {
            var claimsIdentity = (ClaimsIdentity)identity;
            bool.TryParse(claimsIdentity.FindFirst(ClaimNames.IsBanned)?.Value, out bool isBanned);

            return new ApplicationUser
            {
                ChatId = claimsIdentity.FindFirst(ClaimNames.ChatId)?.Value,
                Email = claimsIdentity.FindFirst(ClaimNames.Email)?.Value,
                Metadata = claimsIdentity.FindFirst(ClaimNames.Metadata)?.Value,
                UserId = claimsIdentity.FindFirst(ClaimNames.UserId)?.Value,
                Role = claimsIdentity.FindFirst(ClaimTypes.Role)?.Value,
                IsBanned = isBanned
            };
        }
    }
}