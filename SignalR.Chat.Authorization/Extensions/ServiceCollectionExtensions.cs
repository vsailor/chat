﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace SignalR.Chat.Authorization.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void RegisterAuthorizationDependencies(this IServiceCollection services, IConfigurationRoot configuration)
        {
            services.Configure<AuthorizationOptions>(configuration.GetSection("Authorization"));
        }

        public static IServiceCollection AddTokenProviderService(this IServiceCollection services, IConfiguration configuration,
            Func<MessageReceivedContext, Task> onMessageReceivedJwtBearerEvents = null)
        {
            services.AddAuthorization();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidIssuer = configuration["Authorization:Domain"],
                        ValidAudience = configuration["Authorization:Domain"],
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(configuration["Authorization:AccessTokenSecureKey"]))
                    };

                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            var accessToken = context.Request.Query["access_token"];

                            var path = context.HttpContext.Request.Path;
                            if (!string.IsNullOrEmpty(accessToken) && path.StartsWithSegments("/chat-hub"))
                            {
                                context.Token = accessToken;
                            }

                            return Task.CompletedTask;
                        }
                    };

                    if (onMessageReceivedJwtBearerEvents != null)
                    {
                        options.Events.OnMessageReceived = onMessageReceivedJwtBearerEvents;
                    }
                });

            return services;
        }
    }
}