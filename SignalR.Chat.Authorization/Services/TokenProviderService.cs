﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SignalR.Chat.Authorization.Abstracts;
using SignalR.Chat.Authorization.Models;

namespace SignalR.Chat.Authorization.Services
{
    public class TokenProviderService : ITokenProviderService
    {
        private readonly AuthorizationOptions _authorizationOptions;
        private readonly ILogger<TokenProviderService> _logger;

        public TokenProviderService(IOptions<AuthorizationOptions> authorizationOptions, ILogger<TokenProviderService> logger)
        {
            _authorizationOptions = authorizationOptions.Value;
            _logger = logger;
        }

        private IReadOnlyList<Claim> GetUserClaims(ApplicationUser userClaimsModel)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimNames.UserId, userClaimsModel.UserId ?? string.Empty),
                new Claim(ClaimNames.Metadata, userClaimsModel.Metadata ?? string.Empty),
                new Claim(ClaimNames.Email, userClaimsModel.Email ?? string.Empty),
                new Claim(ClaimNames.ChatId, userClaimsModel.ChatId ?? string.Empty),
                new Claim(ClaimTypes.Role, userClaimsModel.Role ?? string.Empty),
                new Claim(ClaimNames.IsBanned, userClaimsModel.IsBanned.ToString()),
            };

            return claims;
        }

        private IReadOnlyList<Claim> GetUserClaims(List<Claim> existingClaims)
        {
            bool.TryParse(existingClaims.Find(x => x.Type == ClaimNames.IsBanned)?.Value, out bool isBanned);
            var accessTokenModel = new ApplicationUser
            {
                UserId = existingClaims.Find(x => x.Type == ClaimNames.UserId)?.Value,
                Metadata = existingClaims.Find(x => x.Type == ClaimNames.Metadata)?.Value,
                Email = existingClaims.Find(x => x.Type == ClaimNames.Email)?.Value,
                ChatId = existingClaims.Find(x => x.Type == ClaimNames.ChatId)?.Value,
                Role = existingClaims.Find(x => x.Type == ClaimTypes.Role)?.Value,
                IsBanned = isBanned
            };

            return GetUserClaims(accessTokenModel);
        }

        public string GenerateToken(ApplicationUser tokenData)
        {
            return CreateToken(GetUserClaims(tokenData));
        }

        public string RefreshToken(string accessToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_authorizationOptions.AccessTokenSecureKey));

            try
            {
                var principal = tokenHandler.ValidateToken(accessToken, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = key,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = _authorizationOptions.Domain,
                    ValidAudience = _authorizationOptions.Domain,
                    ValidateLifetime = false
                }, out var securityToken);

                var jwtSecurityToken = securityToken as JwtSecurityToken;
                if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256,
                    StringComparison.InvariantCultureIgnoreCase))
                {
                    return null;
                }

                var oldClaims = GetUserClaims(principal.Claims.ToList());
                return CreateToken(oldClaims);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.ToString());
                return null;
            }
        }

        private string CreateToken(IEnumerable<Claim> claims)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_authorizationOptions.AccessTokenSecureKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: _authorizationOptions.Domain,
                audience: _authorizationOptions.Domain,
                claims: claims,
                expires: DateTime.UtcNow.AddDays(7),
                signingCredentials: creds);

            var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);
            return jwtToken;
        }
    }
}