﻿using SignalR.Chat.Authorization.Models;

namespace SignalR.Chat.Authorization.Abstracts
{
    public interface ITokenProviderService
    {
        string GenerateToken(ApplicationUser tokenData);
        string RefreshToken(string accessToken);
    }
}