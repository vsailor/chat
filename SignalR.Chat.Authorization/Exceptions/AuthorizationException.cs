﻿using System.Net;
using SignalR.Chat.Common;

namespace SignalR.Chat.Authorization.Exceptions
{
    public class AuthorizationException : BaseException
    {
        public AuthorizationException(string message, string errorCode, HttpStatusCode statusCode) : base(message, errorCode, statusCode)
        {
        }
    }
}