﻿namespace SignalR.Chat.Authorization
{
    public class AuthorizationOptions
    {
        public string ClientSecret { get; set; }

        public string AccessTokenSecureKey { get; set; }

        public string ApiKey { get; set; }

        public string Domain { get; set; }
    }
}